package main

import (
	"context"
	"fmt"

	pb "github.com/DidierStockmans/shopping-list-service/proto/list"
)

tyhe handler struct{}

func (h *handler) CreateList(ctx context.Context, req *pb.List, res *pb.ListResponse) error {
	fmt.Println("blablab")

	return nil
}

func (h *handler) UpdateList(ctx context.Context, req *pb.List, res *pb.ListResponse) error {
	fmt.Println("UpdateList")
	return nil
}

func (h *handler) DeleteList(ctx context.Context, req *pb.List, res *pb.ListResponse) error {
	fmt.Println("DeleteList")
	return nil
}

func (h *handler) AddItem(ctx context.Context, req *pb.Item, res *pb.ItemResponse) error {
	fmt.Println("AddItem")

	res.Success = true
	res.Item = nil

	return nil
}

func (h *handler) UpdateItem(ctx context.Context, req *pb.Item, res *pb.ItemResponse) error {
	fmt.Println("UpdateItem")
	return nil
}

func (h *handler) DeleteItem(ctx context.Context, req *pb.Item, res *pb.ItemResponse) error {
	fmt.Println("DeleteItem")
	return nil
}
