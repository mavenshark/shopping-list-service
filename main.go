package main

import (
	"log"

	pb "github.com/DidierStockmans/shopping-list-service/proto/list"
	"github.com/micro/go-micro"
)

func main() {
	srv := micro.NewService(micro.Name("shopping.list.service"))

	srv.Init()

	// Register Handler
	pb.RegisterListServiceHandler(srv.Server(), new(service))

	// Run server
	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}
