module github.com/DidierStockmans/shopping-list-service

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.8.2
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
)
